/**
*@author Nguyen Van Quang Huy
*bai toan tim uoc chung lon nhat
*/

public class GCD{
	public static void main (String[] args){
		int x1 = Integer.parseInt (args[0]) ;
		int y1 = Integer.parseInt (args[1]) ;
		
		int x = Math.max (x1,y1) ;
		int y = Math.min (x1,y1) ;
		
		int tg ;
		while (x%y != 0)
		{
			tg = x%y ;
			x = tg ;
			y = x ;
		}
		
		System.out.println (y) ;
	}
}