/**
*@author Nguyen Van Quang Huy
*bai tap tim so cac nguyen to nho hon 10^7
*/

public class PrimeCounter{
	public static void main (String[] args){
		/**
		*su dung mang gom 2 gia tri 0 1 de danh dau 10^7 so dau tien , trong do 0 la so nguyen to 
		*coi mang 10^7 so la 1 sang, so nguyen to con lai tren sang
		*su dung 2 bien chay i , j 
		*voi moi i chay j tu 2 toi 10^7/i de loai bo toan bo cac so la boi cua i khoi sang
		*bien i chi chay toi sqrt (10^7) vi chac chan cac i lon hon deu da duoc danh dau
		*su dung 1 so lenh break de tranh tran mang 
		*/
		
		int n = Integer.parseInt (args[0]) ;
		
		int dd[] = new int[10000004] ;
		//danh dau tat ca cac so tren sang
		for ( int i = 0 ; i <= 10000001 ; i++ ) dd[i] = 0;
		dd[0] = 1 ;
		dd[1] = 1 ;
		
		int i = 2 ;
		int j = 2 ;
		
		//cac lenh break de tranh tran mang 
		while ( (long) (i * i) <= 10000000 )
		{
			if (i*j > 10000000) break ;
			
			//tat ca cac so co uoc so deu bi loai bo khoi sang
			while (i*j <= 10000000 )
			{
				dd[i*j] = 1 ;
				j ++ ;
				if ((i*j) > 10000000 ) break ;
			}
			i ++ ;
			if ( (long ) ( i * i ) > 10000000 ) break ;
		}
		
		
		for ( i = 1 ; i <= n ; i ++ )
			if (dd[i] == 0) System.out.print (i + " ") ;
		
		
		
	}
}