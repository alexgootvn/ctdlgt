/**
*@author Nguyen Van Quang Huy
*bai tap ve in ra bang N x N dau sao
*/

public class CheckerBoard {
	public static void main (String[] args){
		int n = Integer.parseInt (args[0]) ;
		for ( int i = 1 ; i <= n ; i ++ ){
			if ( i % 2 == 0) System.out.print (" ") ;
			for ( int j = 1 ; j <= n ; j ++ )
				System.out.print ("* ") ;
			System.out.print ("\n") ;
		}
	}
}