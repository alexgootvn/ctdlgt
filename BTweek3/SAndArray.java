/*
*@author Nguyen Van Quang Huy
*hackerank Sherlock and Array problem
*/

import java.util.Scanner ;

public class SAndArray {
	public static void main (String[] args){
		Scanner scanIn = new Scanner (System.in) ;
		
		int t = scanIn.nextInt () ;				//number of test case 
		
		for ( int i = 1 ; i <= t ; i ++ ){
			int n = scanIn.nextInt () ;			//number of elements in array a
			int[] a = new int[100001] ;

			int sum = 0 ;						//sum of all elements in array a
			int[] sum1 = new int[100001] ;		//sum of elements on a's left and itself
			sum1[0] = 0;
			
			/** read and calculate sum & sum1 */
			for ( int j = 1 ; j <= n ; j ++ ){
				a[j] = scanIn.nextInt () ;
				sum += a[j] ;
				sum1[j] = sum1[j-1] + a[j] ;
			}
			
			/** query if sum of elements on the left equal to elements on the right */
			int ktr = 0 ;
			for ( int j = 1 ; j <= n ; j ++ ){
				int ss1 = sum - sum1[j] ;		//sum of elements on the right
				int ss2 = sum1[j] - a[j] ;		//sum of elements on the left
				if (ss1 == ss2){
					ktr = 1 ;
					break ;
				}
			}
			if ( ktr == 1 ) System.out.println ("YES") ;
			else System.out.println ("NO") ;
					
			
			
		}
	}
}