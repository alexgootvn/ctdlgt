/**
*@author Nguyen Van Quang Huy
*hackerrank Pairs
*/

import java.util.* ;

public class Pairs {
	public static void main (String[] args){
		Scanner scanIn = new Scanner (System.in) ;
		
		int n = scanIn.nextInt () ;
		long k = scanIn.nextInt () ;
		
		long[] a = new long [n] ;
		
		for ( int i = 0 ; i <= n-1 ; i ++ )
			a[i] = scanIn.nextInt () ;
		
		/** sap xep nhanh mang a */
		Arrays.sort (a) ;
	
		int dem = 0 ;
		
		/**
		*voi moi a[i] tim kiem nhi phan trong mang gia tri a[i] + kiem
		*neu ton tai tang bien dem len 1
		*/
		for ( int i = 0 ; i <= n-1 ; i ++ ){
			long tg = a[i] + k ;
			if ( tg > a[n-1] ) break ;   			//chac chan khong ton tai gia tri a[i] +k trong day
			
			int dau = 0 ;
			int cuoi = n - 1 ;
			int j ;
			int ktr = 0 ;
			
			/** tim kiem nhi phan */
			while ( dau < cuoi ){
				if ( tg > a[cuoi] ) break ;
				if ( tg == a[cuoi] ) ktr = 1 ;
				j = (dau + cuoi)/2 ;
				if ( tg > a[j] )
					dau = j ;
				else if ( tg < a[j] )
					cuoi = j;
				else
					ktr = 1 ;
				if ( dau == cuoi -1 ) break ;
				if (ktr == 1) break ;
			}
			
			if ( ktr == 1 ) dem ++ ;
		}
		
		System.out.println (dem) ; 
	}
}