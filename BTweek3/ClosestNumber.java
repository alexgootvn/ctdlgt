/**
*@author Nguyen Van Quang Huy
*hackerrank Closest Number
*/

import java.util.* ;

public class ClosestNumber {
	public static void main (String[] args){
		
		/** nhap n va day a */
		Scanner scanIn = new Scanner (System.in) ;
		int n = scanIn.nextInt () ;
		
		int[] a = new int [n] ;
		
		for ( int i = 0 ; i <= n-1 ; i ++ )
			a[i] = scanIn.nextInt () ;
		
		/** sap xep nhanh a */
		Arrays.sort(a) ;
		
		/** 
		*tim hieu min
		*hieu nho nhat min la hieu cua 2 so lien ke nhau vi tat ca hieu con lai deu lon hon min
		*duyet lan luot de tim hieu nho nhat
		*/
		int min = 200000001 ;
		for ( int i = 0 ; i <= n-2 ; i ++ )
			if ( a[i+1] - a[i] < min ) min = a[i+1] - a[i] ;
		
		/** in ra cac cap co hieu nho nhat */
		for ( int i = 0 ; i <= n-2 ; i ++ )
			if ( a[i+1] - a[i] == min ) System.out.print (a[i] + " " + a[i+1] + " ") ;
	}
}