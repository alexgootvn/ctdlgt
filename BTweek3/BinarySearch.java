/**
*@author Nguyen Van Quang Huy
*Binary Search 
*/

import java.util.* ;
public class BinarySearch {
	public static void main (String[] args){
		Scanner scanIn = new Scanner (System.in) ;
		
		int n = scanIn.nextInt () ;
		int t = scanIn.nextInt () ;
		
		int[] b = new int [n] ;
		Vector<Integer> v = new Vector<Integer> () ;
		int r = 0;
		
		Random rd = new Random() ;
		
		for ( int i = 0 ; i < n ; )
		{
			r = rd.nextInt (n+10) ;
			if (!v.contains(r)){
				v.add (r) ;
				b[i] = r ;
				i++ ;
			}
		}
		
		Arrays.sort (b) ;
		
		for ( int i = 0 ; i < n ; i ++ ) System.out.print (b[i]+" ") ;
		System.out.print("\n") ;
		System.out.println (BSearch (b,t,0,n-1)) ;
	}
	public static int BSearch (int[] a , int tg , int dau , int cuoi ){
		int index = 0 ;
		int ktr = 0 ;
		
		while ( dau < cuoi ){
			if ( tg > a[cuoi] || tg < a[dau]) break ;
			if ( tg == a[cuoi] ) {
				index = cuoi ;
				break ;
			}
				
			index = (dau + cuoi)/2 ;
			if ( tg > a[index] )
				dau = index ;
			else if ( tg < a[index] )
				cuoi = index;
			else
			{
				ktr = 1 ;
				break ;
			}
			if ( dau == cuoi -1 ) break ;
			if (ktr == 1) return index ;
		}
		
		if ( ktr == 0 ) return -1 ;
		else return index ;
	}
}