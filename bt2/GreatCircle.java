/** 
*@author Nguyen Van Quang Huy
*bai tap tinh khoang cach thuc theo hinh cau giua 2 diem 
*/

public class GreatCircle{
	public static void main (String[] args){
		//chuyen cac kinh do vi do ve don vi rad
		double x = Double.parseDouble (args[0]) ;
		double x1 = Math.toRadians (x) ;
		
		double y = Double.parseDouble (args[1]) ;
		double y1 = Math.toRadians (y) ;
		
		x = Double.parseDouble (args[2]) ;
		double x2 = Math.toRadians (x) ;
		
		y = Double.parseDouble (args[3]) ;
		double y2 = Math.toRadians (y) ;
		
		//khoang cach d sai so khoang 0.5 % , khi tinh su dung 1 so bien trung gian (tg)
		
		double tg1 = Math.sin (x1) ;
		double tg2 = Math.sin (x2) ;
		double tg3 = Math.cos (x1) ;
		double tg4 = Math.cos (x2) ;
		double tg5 = Math.cos (y1-y2) ;
		
		double d = 60 * Math.acos (tg1 * tg2 + tg3 * tg4 * tg5) ;
		
		System.out.println (d) ;
		
		//Tinh theo cong thuc chinh xac
		
		double delta = y1 - y2 ;
		double p1 = tg4 * Math.sin (delta) ;
		double p2 = tg3 * tg2 - tg1 * tg4 * Math.cos (delta) ;
		double p3 = tg1 * tg2 + tg3 * tg4 * Math.cos (delta) ;
		d = 60 * Math.atan2 (Math.sqrt (p1*p1 + p2*p2) , p3 ) ;
		System.out.println (d) ;
	}
}