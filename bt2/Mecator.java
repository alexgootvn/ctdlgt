/**
*@author Nguyen Van Quang Huy
*bai tap ve phep chieu Mecator
*/
public class Mecator {
	public static void main (String[] args){
		//vi tuyen x
		double a = Double.parseDouble (args[2]);
		//x0
		double b = Double.parseDouble (args[0]) ;
		//kinh tuyen y
		double c = Double.parseDouble (args[1]) ;
		
		//dua a b c thanh goc theo don vi Rad
		double a1 = Math.toRadians (a) ;
		double b1 = Math.toRadians (b) ;
		double c1 = Math.toRadians (c) ;
		
		/** Tinh toa do x y sau khi su dung phep chieu Mecator */
		
		double x = a1 - b1 ;
		
		double tg = (1 + Math.sin (c)) / (1 - Math.sin (c)) ;
		
		double y = 1/2 * Math.log (tg) ;
		
		System.out.println (x) ;
		System.out.println (y) ;
	}
}