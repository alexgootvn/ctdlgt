public class StdGaussian{
	public static void main (String[] args) {
		
		//dinh nghia so PI
		final double PI = 3.1415 ;
		
		//sinh 2 so u v ngau nhien trong doan 0 1 
		double u = Math.random() ;
		double v = Math.random() ;
		
		//tinh gia tri bieu thuc bang cach tinh tung thanh phan cua bieu thuc 
		double a = Math.sin (2*PI*v) ;
		
		double b = Math.log (u) ;
		
		double c = Math.sqrt (-2*b) ;
		
		//ket qua cuoi cung
		double kq = a * c ;
		
		System.out.println (kq) ;
	} 

}