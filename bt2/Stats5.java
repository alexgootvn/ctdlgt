/**
*@author Nguyen Van Quang Huy
*bai tap sinh 5 so ngau nhien thuoc khoang 0 1 va tinh gia tri lon nhat, nho nhat, trung binh 5 so nay
*/
public class Stats5 {
	public static void main (String[] args){
		double[] a = new double[6] ;
		
		// sinh 5 so ngau nhien trong khoang 0 1 
		for ( int i = 1 ; i <= 5 ; i ++ )
			a[i] = Math.random() ;
		
		//tim max 5 so 
		double max = -1 ;
		for ( int i = 1 ; i <= 5 ; i ++ )
			max = Math.max ( max , a[i] ) ;
		
		//tim min 5 so 
		double min = 2 ;
		for ( int i = 1 ; i <= 5 ; i ++ )
			min = Math.min ( min , a[i] ) ;
		
		for ( int i = 1 ; i <= 5 ; i ++ )
			System.out.print ( a[i] + " ") ;
		
		System.out.print ("\n") ;
		System.out.println (max) ;
		System.out.println (min) ;
	}
}